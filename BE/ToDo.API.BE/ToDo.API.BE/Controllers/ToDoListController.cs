﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using System.Web.Mvc;
//using System.Web.Http;
using System.Net;
using System.Net.Http;
using Microsoft.AspNet.Identity;
//using System.Web.Mvc;
using System.Web.Http;
using ToDo.API.BE.DATA.Entities;
using System.Web.Http.Results;
using System.Threading.Tasks;
using ToDo.API.BE.DATA;
using ToDo.API.BE.DATA.Models;

namespace ToDo.API.BE.Controllers
{
    [Authorize]
    [RoutePrefix("todo")]

    public class ToDoListController : ApiController
    {
        DATA.database_access_layer.db dblayer = new DATA.database_access_layer.db();

        // Now to return all the Tasks
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [HttpGet]
        [Route("tasks")]
        //public JsonResult GetAllToDoList()
        public HttpResponseMessage GetAllTodoList()
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }

                return Request.CreateResponse(HttpStatusCode.OK, ToDoLists.GetTaskInfo());
            }
            catch(Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest,e.Message);
            }

}

        //Now return only a selected task based on the id of the task that is provided
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [HttpGet]
        [Route("tasks/{taskid}")]
        public HttpResponseMessage Get(int taskid)
        {
            try
            { 
                if (taskid <= 0)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid TaskId");
                }

                string userId = System.Web.HttpContext.Current.User.Identity.GetUserId();

                if (string.IsNullOrEmpty(userId))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid UserId");
                }

                //return Request.CreateResponse(HttpStatusCode.OK, Data.Probes.UserProbe(userId, id));
                return Request.CreateResponse(HttpStatusCode.OK, ToDoLists.GetTaskInfo(taskid));
                //return Json(ToDoLists.GetTaskInfo(taskid));
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        // Now we need a Post Action in order to Create a new ToDo Task
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [HttpPost]
        [Route("insert/{title},{summary},{datecompleted}")]
        public IHttpActionResult AddToDoTask([FromBody]ToDoList td)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                dblayer.Add_ToDoTask(td);

                return Ok("Success");
            }
            catch (Exception)
            {
                return Ok("Something went wrong, try later");
            }

        }

        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("insert/ToDoDetails")]
        [HttpPost]
        public IHttpActionResult PostToDoDetails([FromBody] ToDoList td)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            dblayer.Add_ToDoTask(td);
            //dblayer.SaveChanges();

            return Ok(td);
        }

        //public void PostToDoTask(string title, string summary, string dateCompleted)
        //{
        //    try
        //    {
        //        if (string.IsNullOrEmpty(title))
        //        {
        //            Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid Title");
        //        }

        //        if (string.IsNullOrEmpty(summary))
        //        {
        //            Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid Summary");
        //        }

        //        if (string.IsNullOrEmpty(dateCompleted))
        //        {
        //            Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid Summary");
        //        }

        //        DateTime dtdateCompleted;
        //        dtdateCompleted = Convert.ToDateTime(dateCompleted.ToString());


        //        Request.CreateResponse(HttpStatusCode.OK, ToDoLists.PostTaskInfo(title, summary, dtdateCompleted));
        //    }
        //    catch (Exception e)
        //    {
        //        Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
        //    }
        //}

        // Now we need a Put Action in order to Update a specified ToDo Task
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [HttpPut]
        [Route("tasks/update/{taskid}")]
        public HttpResponseMessage PutToDoTask(int taskid, string title, string summary, DateTime dateCompleted)
        {
            try
            {
                if (string.IsNullOrEmpty(title))
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid Title");
                }

                if (string.IsNullOrEmpty(summary))
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid Summary");
                }

                return Request.CreateResponse(HttpStatusCode.OK, ToDoLists.PutTaskInfo(taskid, title, summary, dateCompleted));
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
            }
        }

        //// Now we need a Delete Action in order to Remove a specified ToDo Task
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [HttpDelete]
        [Route("tasks/delete/{taskid}")]
        public async Task<IHttpActionResult> Delete(int taskid)
        {
            using (TODO_DBEntities db = new TODO_DBEntities())
            {
                //Now just need to pass the sproc the param of which task to "delete"
                db.Database.SqlQuery<TODO_DBEntities>("usp_DeleteTaskInfo", taskid);
                await db.SaveChangesAsync();
            }
            return Ok();
                
        }

        //public string DeleteToDoTask(int taskid)
        //{
        //    try
        //    {
        //        if (taskid <= 0)
        //        {
        //            return "Invalid Task Id";
        //        }
        //        //else
        //        //{

        //        ToDoLists.DeleteTaskInfo(taskid);
        //        return "Task was removed";
        //        //}
        //    }
        //    catch(Exception e)
        //    {
        //        return "Error occured: " + e.Message.ToString();
        //    }
        //}


        //[HttpDelete]
        //public async Task<IHttpActionResult> Delete(int taskid)
        //{
        //    using (TODO_DBEntities db = new TODO_DBEntities())
        //    {
        //        //Now just need to pass the sproc the param of which task to "delete"
        //        db.Database.SqlQuery<TODO_DBEntities>("usp_DeleteTaskInfo", taskid);
        //        await db.SaveChangesAsync();
        //    }
        //    return Ok();
        //}

        //public HttpResponseMessage DeleteToDoTask(int taskid)
        //{
        //    if (taskid <= 0)
        //    {
        //        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid Task Id");
        //    }

        //    return Request.CreateResponse(HttpStatusCode.OK, ToDoLists.DeleteTaskInfo(taskid));
        //}


        ////// GET: ToDoList
        ////public ActionResult Index()
        ////{
        ////    return View();
        ////}

        //// GET: ToDoList/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        //// GET: ToDoList/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: ToDoList/Create
        //[HttpPost]
        //public ActionResult Create(FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add insert logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: ToDoList/Edit/5
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        //// POST: ToDoList/Edit/5
        //[HttpPost]
        //public ActionResult Edit(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add update logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: ToDoList/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        //// POST: ToDoList/Delete/5
        //[HttpPost]
        //public ActionResult Delete(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add delete logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
