﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ToDo.API.BE.Startup))]
namespace ToDo.API.BE
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
