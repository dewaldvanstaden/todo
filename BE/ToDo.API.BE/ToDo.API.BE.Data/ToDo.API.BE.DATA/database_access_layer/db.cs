﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using ToDo.API.BE.DATA.Models;
using System.Collections.Specialized;

namespace ToDo.API.BE.DATA.database_access_layer
{
    public class db
    {

        //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString);
        //SqlConnection con = System.Configuration.ConfigurationManager.ConnectionStrings["Test"].ConnectionString;
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ToDoDB"].ConnectionString);

        //customer cs = new customer();
        TODO_DBEntities td = new TODO_DBEntities();

        public void Add_ToDoTask(ToDoList td)

        {

            SqlCommand com = new SqlCommand("usp_PostTaskInfo", con);

            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@TTitle", td.TTitle);
            com.Parameters.AddWithValue("@TSummary", td.TSummary);
            com.Parameters.AddWithValue("@dateCompleted", td.dateCompleted);

            con.Open();
            com.ExecuteNonQuery();
            con.Close();

        }

    }

}
