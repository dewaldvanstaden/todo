﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDo.API.BE.DATA.Models;

namespace ToDo.API.BE.DATA.Entities
{
    public static class ToDoLists
    {
        // Retrieve the details of all the tasks that are currenlty captured within the dbase
        public static List<ToDoList> GetTaskInfo()
        {
            using (TODO_DBEntities db = new TODO_DBEntities())
            {
                var task = db.usp_GetTaskInfoAll();

                // Task details have been found, continue
                //ToDoList u = new ToDoList();
                List<ToDoList> tdToDoList = new List<ToDoList>();

                if (task != null)
                {
                    foreach (var todo in task)
                    {
                        ToDoList u = new ToDoList();

                        u.TTitle = todo.TTitle;
                        u.TSummary = todo.TSummary;
                        u.dateCreated = todo.dateCreated ?? new DateTime();
                        //u.RS = task.RS == null;// ? false : task.RS.Value == 1;

                        tdToDoList.Add(u);
                    }
                }

                return tdToDoList;
            }
        }

        // Retrieve the details of a specific task
        public static ToDoList GetTaskInfo(int taskid)
        {
            using (TODO_DBEntities db = new TODO_DBEntities())
            {
                var task = db.usp_GetTaskInfo(taskid).FirstOrDefault();

                // Task details have been found, continue
                ToDoList u = new ToDoList();

                if (task != null)
                {
                    u.TTitle = task.TTitle;
                    u.TSummary = task.TSummary;
                    u.dateCreated = task.dateCreated ?? new DateTime();
                    //u.RS = task.RS == null;// ? false : task.RS.Value == 1;
                }

                return u;
            }
        }

        // Update the details of a specific task with the PUT action
        // Need to check what exactly needs to be done here
        public static bool PutTaskInfo(int taskid, string title, string summary, DateTime dateCompleted)
        {
            using (TODO_DBEntities db = new TODO_DBEntities())
            {
                //Now just need to pass the sproc the param of which task to "update"
                db.Database.SqlQuery<TODO_DBEntities>("usp_PutTaskInfo", taskid, title, summary, dateCompleted);

                return true;
            }
        }

        // Insert the details of a new task with the POST action
        // Need to check what exactly needs to be done here
        public static int PostTaskInfo(string title, string summary, DateTime dateCompleted)
        {
            using (TODO_DBEntities db = new TODO_DBEntities())
            {
                //var task = db.usp_PostTaskInfo(out taskid, title, summary, dateCompleted).FirstOrDefault();

                //Now just need to pass the sproc the param of which task to "Inser"
                db.Database.SqlQuery<TODO_DBEntities>("usp_PostTaskInfo", title, summary, dateCompleted);

                //TODO_DBEntities context = new TODO_DBEntities();
                //int wid = db.usp_PostTaskInfo(taskid, title, summary, dateCompleted);
                
                //Need to return the id of the latest inserted record...
                return 1;
            }
        }

        // Delete a specific task
        // Need to check what exactly needs to be done here
        //public static ToDoList DeleteTaskInfo(int taskid)
        public static bool DeleteTaskInfo(int taskid)
        {
            using (TODO_DBEntities db = new TODO_DBEntities())
            {
                //Now just need to pass the sproc the param of which task to "delete"
                db.Database.SqlQuery<TODO_DBEntities>("usp_DeleteTaskInfo", taskid);

                return true;
            }
        }
    }
}
