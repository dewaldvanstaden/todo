﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDo.API.BE.DATA.Models
{
    public class ToDoList
    {
        [Key]
        public int ToDoId { get; set; }
        [Required(ErrorMessage = "Title is required.")]
        public string TTitle { get; set; }
        [Required(ErrorMessage = "Summary is required.")]
        public string TSummary { get; set; }
        [Required(ErrorMessage = "Completion Date is required.")]
        public DateTime dateCompleted { get; set; }
        public DateTime dateCreated { get; set; }
        public DateTime LastLogin { get; set; }
        public bool RS { get; set; }
    }
}
