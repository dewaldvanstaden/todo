USE [master]
GO

/****** Object:  Database [TODO_DB]    Script Date: 03/12/2017 22:20:55 ******/
CREATE DATABASE [TODO_DB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'TODO_DB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQL\MSSQL\DATA\TODO_DB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'TODO_DB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQL\MSSQL\DATA\TODO_DB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO

ALTER DATABASE [TODO_DB] SET COMPATIBILITY_LEVEL = 140
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [TODO_DB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [TODO_DB] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [TODO_DB] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [TODO_DB] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [TODO_DB] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [TODO_DB] SET ARITHABORT OFF 
GO

ALTER DATABASE [TODO_DB] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [TODO_DB] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [TODO_DB] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [TODO_DB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [TODO_DB] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [TODO_DB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [TODO_DB] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [TODO_DB] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [TODO_DB] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [TODO_DB] SET  DISABLE_BROKER 
GO

ALTER DATABASE [TODO_DB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [TODO_DB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [TODO_DB] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [TODO_DB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [TODO_DB] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [TODO_DB] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [TODO_DB] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [TODO_DB] SET RECOVERY FULL 
GO

ALTER DATABASE [TODO_DB] SET  MULTI_USER 
GO

ALTER DATABASE [TODO_DB] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [TODO_DB] SET DB_CHAINING OFF 
GO

ALTER DATABASE [TODO_DB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [TODO_DB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO

ALTER DATABASE [TODO_DB] SET DELAYED_DURABILITY = DISABLED 
GO

ALTER DATABASE [TODO_DB] SET QUERY_STORE = OFF
GO

USE [TODO_DB]
GO

ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO

ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO

ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO

ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO

ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO

ALTER DATABASE [TODO_DB] SET  READ_WRITE 
GO


--
-- Verify that the table does not already exist.  
--
IF OBJECT_ID ( 'tblToDo', 'T' ) IS NOT NULL   
    DROP TABLE tblToDo;  
GO  
CREATE TABLE [dbo].[tblToDo]
(
	[ToDoId] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY, 
    [TTitle] VARCHAR(250) NULL, 
    [TSummary] VARCHAR(1000) NULL, 
    [dateCompleted] DATE NULL, 
    [dateCreated] DATE NULL DEFAULT(getdate()), 
    [RS] TINYINT NULL DEFAULT 1, 
    [dateEdited] DATE NULL, 
    [dateRemoved] DATE NULL
)
Go


--
-- Verify that the stored procedure does not already exist.  
--
IF OBJECT_ID ( 'usp_GetTaskInfoAll', 'P' ) IS NOT NULL   
    DROP PROCEDURE usp_GetTaskInfoAll;  
GO  
--
-- Create procedure to retrieve task information.  
--
CREATE PROCEDURE usp_GetTaskInfoAll  
AS  
BEGIN TRY  
    -- Created by:	DvS
	-- Date:		03/12/2017
    -- Reason:		Select the details, of all the ToDo Task.  
    SELECT ToDoId,TTitle,TSummary,dateCompleted,dateCreated
	from tblToDo
	where RS = 1;  
END TRY  
BEGIN CATCH  
    SELECT  
        ERROR_NUMBER() AS ErrorNumber  
        ,ERROR_SEVERITY() AS ErrorSeverity  
        ,ERROR_STATE() AS ErrorState  
        ,ERROR_PROCEDURE() AS ErrorProcedure  
        ,ERROR_LINE() AS ErrorLine  
        ,ERROR_MESSAGE() AS ErrorMessage;  
END CATCH;  
GO    

--
-- Verify that the stored procedure does not already exist.  
--
IF OBJECT_ID ( 'usp_GetTaskInfo', 'P' ) IS NOT NULL   
    DROP PROCEDURE usp_GetTaskInfo;  
GO  
--
-- Create procedure to retrieve task information.  
--
CREATE PROCEDURE usp_GetTaskInfo  
@ToDoID int
AS  
BEGIN TRY  
	-- Created by:	DvS
	-- Date:		03/12/2017
    -- Reason:		Select the details, of a specified ToDo Task.  
    SELECT ToDoId,TTitle,TSummary,dateCompleted,dateCreated
	from tblToDo
	where ToDoId = @ToDoID
		and RS = 1;  
END TRY  
BEGIN CATCH  
    SELECT  
        ERROR_NUMBER() AS ErrorNumber  
        ,ERROR_SEVERITY() AS ErrorSeverity  
        ,ERROR_STATE() AS ErrorState  
        ,ERROR_PROCEDURE() AS ErrorProcedure  
        ,ERROR_LINE() AS ErrorLine  
        ,ERROR_MESSAGE() AS ErrorMessage;  
END CATCH;  
GO  

--
-- Verify that the stored procedure does not already exist.  
--
IF OBJECT_ID ( 'usp_PostTaskInfo', 'P' ) IS NOT NULL   
    DROP PROCEDURE usp_PostTaskInfo;  
GO  
--
-- Create procedure to insert task information.  
--
CREATE PROCEDURE usp_PostTaskInfo  
@ToDoID int output
,@TTitle varchar(250)
,@TSummary varchar(2000)
,@dateCompleted date
AS  
BEGIN TRY  
	-- Created by:	DvS
	-- Date:		03/12/2017
    -- Reason:		Insert a new ToDo Task, and return the newly inserted id.  

	insert into tblToDo
	(TTitle,TSummary,dateCompleted,dateCreated,RS)
	values
	(@TTitle,@TSummary,@dateCompleted,getdate(),1)
	
	-- return the ID of the task, just incase a user wants to see the id of the task that was just created.
    SELECT @ToDoID = @@IDENTITY;  

END TRY  
BEGIN CATCH  
    SELECT  
        ERROR_NUMBER() AS ErrorNumber  
        ,ERROR_SEVERITY() AS ErrorSeverity  
        ,ERROR_STATE() AS ErrorState  
        ,ERROR_PROCEDURE() AS ErrorProcedure  
        ,ERROR_LINE() AS ErrorLine  
        ,ERROR_MESSAGE() AS ErrorMessage;  
END CATCH;  
GO  

--
-- Verify that the stored procedure does not already exist.  
--
IF OBJECT_ID ( 'usp_PutTaskInfo', 'P' ) IS NOT NULL   
    DROP PROCEDURE usp_PutTaskInfo;  
GO  
--
-- Create procedure to update task information.  
--
CREATE PROCEDURE usp_PutTaskInfo  
@ToDoID int
,@TTitle varchar(250)
,@TSummary varchar(2000)
,@dateCompleted date
AS  
BEGIN TRY  
	-- Created by:	DvS
	-- Date:		03/12/2017
    -- Reason:		Update an existing ToDo Task.  

	update tblToDo
	set TTitle = @TTitle
		,TSummary = @TSummary
		,dateCompleted = @dateCompleted
		,dateEdited = getdate()
	where ToDoId = @ToDoID

END TRY  
BEGIN CATCH  
    SELECT  
        ERROR_NUMBER() AS ErrorNumber  
        ,ERROR_SEVERITY() AS ErrorSeverity  
        ,ERROR_STATE() AS ErrorState  
        ,ERROR_PROCEDURE() AS ErrorProcedure  
        ,ERROR_LINE() AS ErrorLine  
        ,ERROR_MESSAGE() AS ErrorMessage;  
END CATCH;  
GO 

--
-- Verify that the stored procedure does not already exist.  
--
IF OBJECT_ID ( 'usp_DeleteTaskInfo', 'P' ) IS NOT NULL   
    DROP PROCEDURE usp_DeleteTaskInfo;  
GO  
--
-- Create procedure to update task information.  
--
CREATE PROCEDURE usp_DeleteTaskInfo  
@ToDoID int
AS  
BEGIN TRY  
	-- Created by:	DvS
	-- Date:		03/12/2017
    -- Reason:		Delete an existing ToDo Task. 
	--				But take note that the DELETE code was commented out, because I would rather 
	--				recommend that we keep tabs on who deletes what task, so we rather update the RS (Record Status) to 0, and the 
	--				dateRemoved to getdate() 

	/*
		delete tblToDo
		where ToDoID = @ToDoID
	*/

	update tblToDo
	set rs = 0
		,dateRemoved = getdate()
	where ToDoId = @ToDoID

END TRY  
BEGIN CATCH  
    SELECT  
        ERROR_NUMBER() AS ErrorNumber  
        ,ERROR_SEVERITY() AS ErrorSeverity  
        ,ERROR_STATE() AS ErrorState  
        ,ERROR_PROCEDURE() AS ErrorProcedure  
        ,ERROR_LINE() AS ErrorLine  
        ,ERROR_MESSAGE() AS ErrorMessage;  
END CATCH;  
GO 




