# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary: Creating, Listing individual, Listing all, Update and removal of ToDo tasks
* Version 1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* First in your package should be a tsql script file, that you can run on your local pc.
* Then with the applications opened,
* First you need to change the sql connections in the differenct projects
	Them being: 
	BE => ToDo.API.BE.DATA
		> App.config: 
			= TODO_DBEntities
			= ToDoDB 
		=> ToDo.API.BE
		> web.config: 
			= TODO_DBEntities
			= ToDoDB 
	FE => index.js located under the js directory
		> change const baseUrl = 'http://localhost:54828/todo'; to the local port that your pc is using.
	
* Testing purposes
http://localhost:54828/todo/1 => returns the specific todo task with id 1
http://localhost:54828/todo/tasks => Returns all the registered tasks

* For the Swagger doc => http://localhost:54828/swagger/docs/v1

* Several nuget packages have been used, but they should download and install on initial built
* Please note:
	That with the FE section, I could not yet get ReactJS to function, and did not get my environment up and running.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact