﻿import React from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import Pusher from "pusher-js";

const baseUrl = 'http://localhost:54828/todo';

const Welcome = ({ onSubmit }) => {
    let usernameInput;
    return (
        <div>
            <p>Enter your Email address and start tasking!</p>
            <form onSubmit={(e) => {
                e.preventDefault();
                onSubmit(usernameInput.value);
            }}>
                <input type="text" placeholder="Enter Email address here" ref={node => {
                    usernameInput = node;
                }}/>
                <input type="submit" value="Join the change" />
            </form>
        </div>
    );
};

const TaskInputForm = ({
    onSubmit
}) => {
    let taskID;
    return (
        <form onSubmit = { e => {
            e.preventDefault();
            onSubmit(taskID.value);
            taskID.value = ""; 
        }}>
            <input type = "text" placeholder = "Task Title" ref = { node => { 
                taskID = node; 
            }} /> 
            <input type="text" placeholder="Task Summary" ref={node => {
                taskID = node;
            }} /> 
            <input type="text" placeholder="Task Completion Date" ref={node => {
                taskID = node;
            }} /> 
            <input type = "submit" value = "Send" / >
        </ form>
    );
};

const CompleteList = ({ task, username }) => (
    <li className='task-detail-li'>
        <strong>@{username}: </strong> {task}
    </li>
);

const ToDoLists = ({ tasks }) => (
    <ul>
        {tasks.map((task, index) => 
            <CompleteList 
                key={index} 
                title={task.Title} 
                summary={task.Summary}
                completedate={task.dateCompleted}/> 
        )}
    </ul>
);


const Task = ({ onSubmit, tasks }) => (
    <div> 
        <ToDoLists tasks={tasks} />
        <TaskInputForm onSubmit={onSubmit}/>
    </div>
);

const App = React.createClass({
    getInitialState() {
        return {
            authorTwitterHandle: "",
            messages: []
        }
    },

    componentDidMount() {
        axios
            .get(`${baseUrl}/tasks`)
            .then(response => {
                this.setState({
                    tasks: response.data
                });
                var pusher = new Pusher('YOUR APP KEY', {
                    encrypted: true
                });
                var taskList = pusher.subscribe('tasks');
                taskList.bind('new_task', task => {
                    this.setState({
                        tasks: this.state.tasks.concat(task)
                    });
                });
            });
    },

    sendTask(taskDetails) {
        axios
            .post(`${baseUrl}/tasks/insert`, {
                text: taskTitle,
                authorTwitterHandle: this.state.authorTwitterHandle
            })
            .catch(() => alert('Something went wrong :('));
    },

    render() {
        if (this.state.authorTwitterHandle === '') {
            return (
                <Welcome onSubmit = { author => 
                    this.setState({
                        authorTwitterHandle: author
                    })
                }/>
            );
        } else {
            return <Task tasks={this.state.ToDoLists} onSubmit={this.ToDoLists} />;
        }
    }
});

ReactDOM.render(<App />, document.getElementById("app"));